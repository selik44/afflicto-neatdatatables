#Examples
To use the examples included in the "examples" directory, first create a database named "ndt" then import the "users.sql" file into the DB using phpmyadmin or similar tool.
Then open up examples/includes/boot.php file and change the Database connection information to suit your DB.
Then, simply point your browser to the example .php files.

For documentation, please visit http://afflicto.net/demo/datatables
