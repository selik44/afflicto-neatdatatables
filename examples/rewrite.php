<?php
#----------------------------------
#   Example 3: Rewrite
#	
#	The rewrite method allows you
#	to bind a closure (anonymous function) to a table column
#	This allows us to alter the output of the email for example.
#
#	In this example, we rewrite the
#	'email' value to echo a mailto://emailValue
#	link instead of plain text.
#
#	Note: The lines that are different here are lines 35 and 42 :)
#----------------------------------
require_once 'includes/boot.php';

use afflicto\neatdatatables\datatable;

$columns = array(
	'id' => '#',
	'first_name' => 'First name',
	'last_name' => 'Last name',
	'email' => 'Email',
	'country' => 'Country',
	'ip_address' => 'IP Address',
);

$dt = new datatable('users', $columns);


#----------------------------------
#    Create a rewrite for the email column
#----------------------------------
$dt->rewrite('email', function($user) {
	return '<a href="mailto://' .$user['email'] .'">' .$user['email'] .'</a>';
});


echo $dt->display();