<?php
#----------------------------------
#	Example 6: jQuery Plugin
#	If you'd like to use interactive functions on the client
#	I have written a jQuery plugin to facilitate some functions.
#----------------------------------
require_once 'includes/boot.php';

use afflicto\neatdatatables\datatable;

$columns = array(
	'id' => '#',
	'first_name' => 'First name',
	'last_name' => 'Last name',
	'email' => 'Email',
	'country' => 'Country',
	'ip_address' => 'IP Address',
);

$dt = new datatable('users', $columns);


#----------------------------------
#    jQuery plugin
# here's how to enable it
datatable::jQuery(true);

# We can set some options for the plugin
# like so: $dt->option('option', 'value');
# For a list of options available, refer to the documentation at afflicto.net/demos/datatables

# Here's one neat option which creates a drop-down menu in the pagination bar, to jump to pages.
$dt->option('jumpMenu', true);


$dt->rewrite('email', function($user) {
	return '<a href="mailto://' .$user['email'] .'">' .$user['email'] .'</a>';
});

$dt->action(function($user) {
	return '<a href="http://example.com/users/edit/' .$user['id'] .'">Edit</a>';
});

$dt->action(function($user) {
	return '<a href="http://example.com/users/delete/' .$user['id'] .'">Delete</a>';
});


$page = 1;
if (isset($_GET['page'])) {
	$page = $_GET['page'];
	if ($page < 1) {
		$page = 1;
	}
}
$dt->paginate(8, $page, 'http://dev/neatdatatables/examples/jquery.php?page={page}');


# This is a bit cleaner, put the table in a variable first, then echo it later :)
$table = $dt->display();

?><!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="../src/css/theme.css">
	<!-- load jquery -->
	<script src="includes/jquery.min.js"></script>

	<!-- ...and the plugin -->
	<script src="../src/js/jQuery.neatDataTables.js"></script>
	<style>
		body {
			font-family: helvetica, sans-serif;
		}
	</style>

</head>
<body>
	<?=$table?>
</body>
</html>