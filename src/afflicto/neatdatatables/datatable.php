<?php
namespace afflicto\neatdatatables;
use PDO;
use Exception;
use afflicto\neatdatatables\html\element;

/**
 * @package NeatDatatables
 * @link http://afflicto.net/demos/datatables
 * @author Petter Thowsen <me@afflicto.net>
 * @copyright Petter Thowsen, 2014
 * @version 0.0.0
 */
class datatable extends element {

	/**
	 * @since 0.0.0
	 */
	private static $pdo;

	/**
	 * @since 0.0.0
	 */
	private static $counter = 1;

	/**
	 * @since 0.0.0
	 */
	private static $jQuery = false;

	/**
	 * These options are given to the jQuery plugin instance
	 * @since 0.0.0
	 */
	protected $options = array();

	/**
	 * The unique ID of this table
	 * @see $counter property
	 * @since 0.0.0
	 */
	protected $uid;

	/**
	 * The name of the table, eg 'users'
	 * @since 0.0.0
	 */
	protected $table;

	/**
	 * An associative array of columns to include.
	 * Elements whose values are NULL are not displayed, but included in the results
	 * @since 0.0.0
	 */
	protected $columns = array();

	/**
	 * The data from the table
	 * @since 0.0.0
	 */
	protected $records;

	/**
	 * @since 0.0.0
	 */
	protected $pagination = false;

	/**
	 * @since 0.0.0
	 */
	protected $perPage = 10;

	/**
	 * @since 0.0.0
	 */
	protected $currentPage = 1;

	/**
	 * @since 0.0.0
	 */
	protected $numRows = null;

	/**
	 * @since 0.0.0
	 */
	protected $paginateLink;

	/**
	 * @since 0.0.0
	 */
	protected $actions = array();

	/**
	 * @since 0.0.0
	 */
	protected $rewrites = array();

	protected $whereClauses = array();

	/**
	 * @since 0.0.0
	 */
	static public function connect($dsn, $username, $password) {
		try {
			static::$pdo = new PDO($dsn, $username, $password);
		} catch (Exception $e) {
			throw new Exception("Datatable failed to connecto DB: " .$e->getMessage(), 1);
		}
	}

	/**
	 * @since 0.0.0
	 */
	static public function getPDO() {
		return static::$pdo;
	}

	/**
	 * @since 0.0.0
	 */
	static public function jQuery($bool = true) {
		static::$jQuery = $bool;
	}

	/**
	 * @since 0.0.0
	 */
	public function option($key, $val = null) {
		if ($val == null) {
			return (isset($this->options[$key])) ? $this->options[$key] : null;
		}else {
			$this->options[$key] = $val;
		}
	}

	/**
	 * @since 0.0.0
	 */
	public function getLimit() {
		return $this->perPage;
	}

	/**
	 * @since 0.0.0
	 */
	public function getOffset() {
		return ($this->currentPage-1) * $this->perPage;
	}

	/**
	 * @since 0.0.0
	 */
	public function getNumPages() {
		return ceil($this->getRows() / $this->perPage);
	}

	/**
	 * @since 0.0.0
	 */
	public function getRows() {
		if (isset($this->numRows)) return $this->numRows;
		$this->numRows = static::$pdo->query('select count(*) from ' .$this->table)->fetchColumn();
		return $this->numRows;
	}

	/**
	 * @since 0.0.0
	 */
	public function __construct($table, $columns) {
		$this->table = $table;
		$this->columns($columns);
		$this->setType('table');
		$this->hasEndTag(true);
		$this->addClass('ndt-table');

		$this->header = new element('thead', true);
		$this->body = new element('tbody', true);
		$this->footer = new element('tfoot', true);

		$this->uid = static::$counter++;
		$this->attr('id', 'ndt-' .$this->uid);
	}

	/**
	 * If the first argument is false, disables pagination entirly.
	 * Otherwise, enables it.
	 * @param int $perpage how many records to display per page
	 * @param int $currentPage the current page
	 * @since 0.0.0
	 */
	public function paginate($perPage, $currentPage) {
		if ($perPage == false) {
			$this->option('pagination', false);
			return;
		}
		$this->option('pagination', true);
		$this->perPage = $perPage;
		$this->currentPage = $currentPage;
	}

	/**
	 * @since 0.2.0
	 */
	public function sort($sortable, $column, $dir) {
		$dir = strtoupper($dir);
		if ($dir !== 'ASC' && $dir !== 'DESC') {
			throw new Exception("Parameter 3 expects either 'ASC' or 'DESC', instead got $dir", 1);
		}

		$this->option('sortable', true);
		$this->option('sortableColumns', $sortable);
		$this->option('sortableColumn', $column);
		$this->option('sortableDir', $dir);
	}

	/**
	 * @since 0.0.0
	 */
	public function action($closure) {
		$this->actions[] = $closure;
		return $this;
	}

	/**
	 * @since 0.0.0
	 */
	public function actions($array) {
		$this->actions = array_merge($array, $this->actions);
		return $this;
	}

	/**
	 * @since 0.0.0
	 */
	public function rewrite($column, $closure = null) {
		if (is_array($column)) {
			foreach($column as $column => $closure) {
				$this->rewrite($column, $closure);
			}
			return $this;
		}
		$this->rewrites[$column] = $closure;
		return $this;
	}

	/**
	 * Alias for rewrite
	 * @since 1.2.0
	 */
	public function rewrites($array) {
		return $this->rewrite($array);
	}

	/**
	 * @since 0.0.0
	 */
	public function columns($array) {
		$this->columns = array_merge($array, $this->columns);
		return $this;
	}

	/**
	 * @since 0.0.0
	 */
	public function where($column, $operator, $value) {
		$this->whereClauses[] = ['column' => $column, 'operator' => trim($operator), 'value' => $value];
		return $this;
	}

	/**
	 * @since 0.0.0
	 */
	private function getRecords() {
		$pdo = static::getPdo();
		$records = array();

		$values = array();

		# generate SQL Query, first we add the columns to select
		$sql = 'SELECT ';
		foreach($this->columns as $machine => $human) {
			$sql .= $machine .', ';
		}
		$sql = rtrim($sql, ', ');
		$sql .= ' FROM ' .$this->table;

		# add the where clauses, using only PDO style prepared statements
		if (count($this->whereClauses) > 0) {
			$sql .= ' WHERE ';
			foreach($this->whereClauses as $clause) {
				$sql .= $clause['column'] .' ' .$clause['operator'] .' ? AND ';
				$values[] = $clause['value'];
			}
			$sql = substr($sql, 0, -5);
		}

		# sortable? if so, add the neccecary ORDER clauses
		if ($this->option('sortable')) {

			# what column are we sorting by?
			$sortBy = $this->option('sortableColumn');

			# is it allowed to sort by this column?
			if (in_array($sortBy, $this->option('sortableColumns'))) {
				# add the SQL
				$sql .= ' ORDER BY ' .$sortBy .' ' .$this->option('sortableDir');
			}
		}

		# if pagination is enabled, add limit and offset clauses
		if ($this->option('pagination')) $sql .= ' LIMIT ' .$this->getLimit() .' OFFSET ' .$this->getOffset();

		# prepare the statement...
		$sth = $pdo->prepare($sql);

		# execute...
		if ($sth->execute($values)) {
			$this->records = $sth->fetchAll();
			return true;
		}else {
			throw new Exception("Failed to get records!" .$pdo->errorInfo(), 1);
			return false;
		}
	}

	public function search($for, $in) {

	}

	/**
	 * @since 0.0.0
	 */
	protected function renderPaginateLink($page, $text) {
		if ($page == $this->currentPage) return '<li><span>' .$text .'</span></li>';
		$link = str_replace('{page}', $page, $this->option('url'), $link);
		$link = str_replace('{column}', $this->option('sortableColumn'), $link);
		$link = str_replace('{direction}', $this->option('sortableDir'), $link);
		return '<li><a href="' .$link .'">' .$text .'</a></li>';
	}

	public function createSortableLink($page, $column, $dir) {
		$link = str_replace('{page}', $page, $this->option('url'));
		$link = str_replace('{column}', $column, $link);
		return str_replace('{direction}', $dir, $link);
	}

	/**
	 * @since 0.0.0
	 */
	public function renderPagination() {
		$cp = $this->currentPage;
		$pp = $this->perPage;
		$numPages = $this->getNumPages();
		$pagesAfterThisOne = $numPages - $cp;
		$str = '<footer class="ndt-pagination">';

		$str .= '<ul>';
		#prev link?
		if ($cp > 1) {
			$str .= $this->renderPaginateLink($cp-1, 'Previous');
		}

		if ($this->option('jumpMenu') == true) {
			$str .= '<li class="ndt-dropdown"><select>';
			for($i = 1; $i<= $numPages; $i++) {
				if ($i == $cp) {
					$str .= '<option selected="selected" value="' .$i .'">' .$i .'</option>';
					continue;
				}
				$str .= '<option value="' .$i .'">' .$i .'</option>';
			}
			$str .= '</select></li>';
		}else {
			$str .= $this->renderPaginateLink($cp, $cp);
		}

		#next link
		if ($cp < $numPages) {
			$str .= $this->renderPaginateLink($cp+1, 'Next');
		}
		$str .= '</ul>';

		$str .= '</footer>';
		return $str;
	}

	/**
	 * @since 0.0.0
	 */
	public function renderHead() {
		$str = '<thead><tr>';

		# loop through columns
		if ($this->option('sortable')) {
			$dir = strtoupper($this->option('sortableDir'));
			$arrowDir = $dir;
			foreach($this->columns as $machine => $human) {
				# skip this column?
				if ($human == null) continue;

				$active = '';
				$dirLink = $dir;
				if ($this->option('sortableColumn') == $machine) {
					$active = 'active ';
					//flip the direction
					if ($dir == 'ASC') {
						$dirLink = 'DESC';
					}else {
						$dirLink = 'ASC';
					}
				}
				$str .= '<th><span class="ndt-title">' .$human .'</span> <a href="' .$this->createSortableLink($this->currentPage, $machine, $dirLink) .'" class="ndt-sortarrow"><div class="' .$active .'arrow ndt-arrow-' .strtolower($arrowDir) .'"></div></a></th>';
			}
		}else {
			foreach($this->columns as $machine => $human) {
				# skip this column?
				if ($human == null) continue;
				$str .= '<th>' .$human .'</th>';
			}
		}
		if (count($this->actions) > 0) {
			$str .= '<th></th>';
		}

		$str .= '</tr></thead>';

		return $str;
	}

	/**
	 * @since 0.0.0
	 */
	public function renderFoot() {
		$str = '<tfoot>';
			$str .= '<tr></tr>';
		$str .= '</tfoot>';
		return $str;
	}

	/**
	 * @since 0.0.0
	 */
	public function renderBody() {
		$str = '<tbody>';
		
		foreach($this->records as $record) {
			$str .= '<tr>';
			# columns
			foreach($this->columns as $machine => $human) {
				# Should this column be skipped?
				if ($human == null) continue;
				$value = $record[$machine];

				# is there a rewrite for this column?
				if (isset($this->rewrites[$machine])) {
					$value = $this->rewrites[$machine]($record);
				}else {
					$value = htmlentities($value);
				}

				$str .= '<td>' .$value .'</td>';
			}
			# any actions?
			if (count($this->actions) > 0) {
				$str .= '<td class="ndt-actions"><ul class="ndt-actions-list">';
				foreach ($this->actions as $action) {
					$str .= '<li>' .$action($record) .'</li>';
				}
				$str .= '</ul></td>';
			}
			$str .= '</tr>';
		}

		$str .= '</tbody>';

		return $str;
	}

	//display Option Macros?
	//pre-defined "options"

	/**
	 * @since 0.0.0
	 */
	public function display() {
		$this->getRecords();

		$str = $this->renderHead();
		$str .= $this->renderFoot();
		$str .= $this->renderBody();

		$this->setContent($str);

		$str = parent::display();

		# is pagination enabled?, if so, add some pagination!
		if ($this->option('pagination') == true) $str .= $this->renderPagination();

		# using jQuery plugin?
		if (static::$jQuery) {
			$str .= '
			<script type="text/javascript">
			$("#' .$this->attr('id') .'").neatDataTables({';
				foreach($this->options as $key => $val) {
					$str .= '' .json_encode($key) .': ' .json_encode($val) .',';
				}
			$str .= '});
			</script>
			';
		}

		return $str;
	}

}