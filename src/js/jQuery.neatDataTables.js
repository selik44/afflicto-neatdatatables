;(function ($, window, document, undefined) {
	
	
	/* method calling logic */
	$.fn.neatDataTables = function() {
		var method = arguments[0];
		if (this.data('neatdatatables')) {
			//grab the method from arguments 0
			
			//remove the first element in the array
			Array.prototype.splice.call(arguments, 0,1);
			
			//prepend data
			Array.prototype.unshift.call(arguments, this.data('neatdatatables'));
			
			if ($.fn.neatDataTables.methods[method]) {
		      	return $.fn.neatDataTables.methods[method].apply( this, arguments);
		    }else {
		    	$.error('Method ' +  method + ' does not exist on jQuery.neatDataTables!');
		    }
		}else {
			console.log('data NOT set');
			if ( $.fn.neatDataTables.methods[method] ) {
		      	return $.fn.neatDataTables.methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		    } else if ( typeof method === 'object' || typeof method === 'string' || ! method ) {
		    	var options = {};
		    	if (typeof method === 'object') {
		    		options = method;
		    	}
		      	this.data('neatdatatables', {'options': $.extend({}, $.fn.neatDataTables.options, options)});
		      	var data = this.data('neatdatatables');
		      	$.fn.neatDataTables.methods['init'].apply( this, [data]);
		      	return this;
		    } else {
		      	$.error('Method ' +  method + ' does not exist on jQuery.neatDataTables!');
		    }
		}
	}

	/* holds available validators */
	$.fn.neatDataTables.validators = {};

	$.fn.neatDataTables.addValidator = function(name, callback) {
		$.fn.neatDataTables.validators[name] = callback;
	}

	/* default ajaxHandler */
	$.fn.neatDataTables.ajaxHandler = function(form, data) {
		form.after(data);
	};

	$.fn.neatDataTables.setAjaxHandler = function(callback) {
		$.fn.neatDataTables.ajaxHandler = callback;
	}

	/* default options */
	$.fn.neatDataTables.options = {
		url: null,
		sortable: false,
		sortableColumn: null,
		sortableDir: null,
	};
	
	/* methods */
	$.fn.neatDataTables.methods = {
		init: function(data) {
			var self = this;
			data.pagination = this.next('.ndt-pagination');
			data.pagination.find('.ndt-dropdown select').change(function(e) {
				window.location.href = self.neatDataTables('createLink', $(this).val(), data.options.sortableColumn, data.options.sortableDir);
			});

			return this;
		},

		createLink: function(data, page, column, dir) {
			return data.options.url.replace('{page}', page).replace('{column}', column).replace('{direction}', dir);
		},
		
		test: function(data) {
			return this;
		},
	}
	
})(jQuery, window, document);